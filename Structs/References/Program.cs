﻿/*
 * Created by SharpDevelop.
 * User: anguyen
 * Date: 24/05/2017
 * Time: 3:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace References
{
	class Program
	{
		public static void Main(string[] args)
		{
			int a = 5;
			int b = 10;
			
			norefChangeAB(a, b);
			Console.WriteLine(a + " " + b);
			
			refChangeAB(ref a, ref b);
			
			Console.Write(a + " " + b);
			Console.ReadKey(true);
		}
		
		static void refChangeAB(ref int aa, ref int bb) {
			aa = 20;
			bb = 40;
		}
		
		static void norefChangeAB(int aa, int bb) {
			aa = 20;
			bb = 40;
		}
		
		static int changeA(int aa) {
			aa = 20;
			return aa;
		}
		
		
	}
}