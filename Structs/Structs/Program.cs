﻿/*
 * Created by SharpDevelop.
 * User: anguyen
 * Date: 24/05/2017
 * Time: 3:09 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Structs
{
	class Program
	{
		public struct Item {
			public string name;
			public float price;
		}
		
		public struct Car {
			//public string make;
			public string model;
			public int wheels;
		}
		
		public static void Main(string[] args)
		{
			Car car1;
			car1.make = "Holden";
			car1.model = "Commodore";
			car1.wheels = 4;
			
			Car[] cars = new Car[2];
			
			cars[0] = car1;
			cars[1].make = "Ford";
			cars[1].model = "Falcon";
			cars[1].wheels = 4;
			
			foreach(Car c in cars) {
				Console.WriteLine(c.make + " " + c.model + " " + c.wheels);
			}
			
			// TODO: Implement Functionality Here
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}