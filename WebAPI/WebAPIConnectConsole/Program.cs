﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIConnectConsole {
    class Program {
        static void Main(string[] args) {
            HttpRequest caller = new WebAPIConnectConsole.HttpRequest();

            Console.ReadKey();

            string url = "http://localhost:11782/api/values/5";

            Console.WriteLine("Sending Request to the server");
            Task<string> res = caller.GetRequest(url);

            Console.WriteLine(res.Result);

            Console.ReadKey();
        }
    }

    class HttpRequest {

        public async Task<string> GetRequest(string url) {            
            HttpClient client = new HttpClient();
            string response = await client.GetStringAsync(url);

            return response;
        }

        public async Task<string> GetRequestTest(string url) {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);

            return response;
        }
    }
}
