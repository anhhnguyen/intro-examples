﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class HelloController : ApiController
    {
        // GET api/hello/?id=5&id2=90
        public string Get(int id, int id2) {
            int ans = id + id2;

            return ans.ToString();
        }

        public string Get(string fname, string sname) {
            return fname + " " + sname;
        }

        public string Get(string fname, string sname, int age) {
            Person p = new Person(fname, sname, age);

            return p.PersonInfo();
        }
    }
}
