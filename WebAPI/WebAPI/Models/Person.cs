﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models {
    public class Person {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public Person(string f, string s, int a) {
            FirstName = f;
            Surname = s;
            Age = a;
        }

        public string PersonInfo() {
            return this.FirstName + " " + this.Surname + " is " + this.Age.ToString() + " years old";
        }
    }
}