﻿/*
 * Created by SharpDevelop.
 * User: anguyen
 * Date: 26/04/2017
 * Time: 3:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace ten_numbers
{
	class Program
	{
		public static int[] getNumbers() {
			int[] numbers = new int[10];
			int count = 0;
			
			while(count < 10) {
				Console.Write("Please enter a number: ");
				int num = Int32.Parse(Console.ReadLine());
				
				numbers[count] = num;
				count++;				 
			}
			
			return numbers;
			
		} // end getNumbers
		
		public static void printNumbers(int[] numbers) {
			int count = 0;
			
			Console.WriteLine();
			while(count < numbers.Length) {
				Console.Write(numbers[count] + ", ");
				count++;
			}
			Console.WriteLine();
		}
		
		public static int getTotal(int[] numbers) {
			int count = 0;
			int total = 0;
			
			while(count < 10) {
				total += numbers[count];
				count++;
			}
			
			return total;
		}
		
		public static float getAverage(int[] numbers) {
			float avg = ((float)getTotal(numbers) / 10f);
			
			return avg;
		}
		
		public static void Main(string[] args)
		{
			
			int selection = -1;
			int[] main_numbers;
			
			main_numbers = getNumbers();
			printNumbers(main_numbers);
			
			while(selection != 0) {
				Console.WriteLine("---- Menu ----");
				Console.WriteLine("(1) Average");
				Console.WriteLine("(2) Total");
				Console.WriteLine("(0) Exit");
				Console.WriteLine("------------");
				
				// read string from console and convert to an integer
				// no validation
				selection = Int32.Parse(Console.ReadLine());
				
//				if(selection == 1) {
//					Console.WriteLine("Run getAverage function");
//				}
//				if(selection == 0) {
//					Console.WriteLine("Exit");
//				}
				
				switch (selection) {
					case 0:
						Console.WriteLine("Exit");
						break;
						
					case 1:
						Console.WriteLine("Average is " + getAverage(main_numbers));
						break;
						
					case 2:
						Console.WriteLine("Total is " + getTotal(main_numbers));
						break;
					default:
						Console.WriteLine("Please enter a valid option");
						break;
				}
				
			}
			
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}