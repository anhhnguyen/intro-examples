﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Listbox {
    public partial class Form1 :Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {

            if(textBox1.Text != "") {
                listBox1.Items.Add(textBox1.Text);
                textBox1.Clear();
            } else {
                MessageBox.Show("Can't add nothing");
            }
        }

        private void button2_Click(object sender, EventArgs e) {

            if(listBox1.SelectedIndex != -1) {
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
            else {
                MessageBox.Show("Something must be selected");
            }

        }
    }
}
